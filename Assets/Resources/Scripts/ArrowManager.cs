﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ArrowManager : MonoBehaviour {

	[Header("Arrow Structure")]
	public GameObject arrowPrefab;
	public GameObject effectPrefab;
	
	[Header("Bow")]
	public GameObject bow;
	
	[Header("Reload Bar")]
	public Slider reloadBar;
	
	[Header("Pool Manager")]
	public GameObject poolManager;

	bool _isSelected = false, _isReady = true, _isCooling = false;
	
	float _cdTimer = 0f, _maxCDTime;
	
	Coroutine _delayedRequestRoutine;
	
	PoolManager _pmScript;
	BowBehaviour _bowScript;
	
	void Start () {
		_maxCDTime = arrowPrefab.GetComponent<ArrowBehaviour>().reloadTime;
		_bowScript = bow.GetComponent<BowBehaviour>();
		_pmScript = poolManager.GetComponent<PoolManager>();
	}
	
	void Update () {
		if (_isCooling) {
			_cdTimer += Time.deltaTime;
			reloadBar.value = _cdTimer / _maxCDTime;
			if (_cdTimer >= _maxCDTime) {
				_cdTimer = _maxCDTime;
				_isReady = true;
				_isCooling = false;
			}
		}
	}
	
	public bool SupplyArrow () {
		if (_isReady) {
			StartCoroutine(QueuedSupply(0f));
			return true;
		}
		if (_delayedRequestRoutine == null) {
			_delayedRequestRoutine = StartCoroutine(QueuedSupply(_maxCDTime - _cdTimer));
		}
		return false;
	}
	
	public void OnPressed () {
		if (!_isSelected) {
			_bowScript.UpdateArrowSupplier(this);
			_bowScript.UnloadArrow();
			_bowScript.UpdatePrefabNames(arrowPrefab, effectPrefab);
			StartCoroutine(QueuedSupply(0f));
			_isSelected = true;
		}
	}
	
	public void OnReleased () {
		if (_isSelected) {
			if (_delayedRequestRoutine != null) {
				StopCoroutine(_delayedRequestRoutine);
			}
			_isSelected = false;
		}
	}
	
	IEnumerator QueuedSupply (float waitTime) {
		yield return new WaitForSeconds(waitTime);
		_bowScript.SetArrowType(_pmScript.RequestItem(arrowPrefab), _pmScript.RequestItem(effectPrefab));
		_isCooling = true;
		_isReady = false;
		_cdTimer = 0f;
		_delayedRequestRoutine = null;
	}
	
}

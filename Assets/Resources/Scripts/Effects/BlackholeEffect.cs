﻿using UnityEngine;
using System.Collections;

public class BlackholeEffect : Effect {

	[SerializeField]
	private AnimationCurve powerCurve;
	[SerializeField]
	private float damage;
	[SerializeField]
	private float timeToLive;
	[SerializeField]
	private GameObject suckPrefab;
	
	private float currTime = 0f, maxDistance;
	private bool startNow = false;

	void OnEnable () {
		currTime = timeToLive;
	}

	new void Start () {
		base.Start();
		maxDistance = gameObject.GetComponent<BoxCollider2D>().size.x / 2f + 0.5f;
	}
	
	void Update () {
		if (startNow) {
			currTime -= Time.deltaTime;
			if (currTime <= 0f) {
				StopEffect();
			}
		}
	}
	
	void OnTriggerEnter2D (Collider2D other) {
		
	}
	
	void OnTriggerStay2D (Collider2D other) {
		if (other.gameObject.tag == "Enemy") {
			EnemyStatusManager targetStatusManager = other.gameObject.GetComponent<EnemyStatusManager>();
			if (!targetStatusManager.GetSuckStatus()) {
				GameObject suckObj = pmScript.RequestItem(suckPrefab);
				suckObj.GetComponent<SuckEffect>().Setup(this, currTime, damage);
				targetStatusManager.Inflict(suckObj, EnemyStatusManager.Statuses.Suck);
			}
		}
	}
	
	protected override void ResetToDefault () {
		currTime = 0f;
		startNow = false;
		gameObject.GetComponent<SpriteRenderer>().enabled = false;
		gameObject.GetComponent<BoxCollider2D>().enabled = false;
	}
	
	protected override void StopEffect (bool isNaturalRemove) {
		ResetToDefault();
		ReturnToPool();
	}
	
	public override void StartEffect () {
		gameObject.GetComponent<BoxCollider2D>().enabled = true;
		gameObject.GetComponent<SpriteRenderer>().enabled = true;
		startNow = true;
	}
	
	public float GetPullStrength (float x) {
		return powerCurve.Evaluate(x);
	}
	
	public float MaxDistance {
		get { return maxDistance; }
	}
	
	IEnumerator Remove () {
		yield return new WaitForSeconds(timeToLive);
		StopEffect(true);
	}
	
}

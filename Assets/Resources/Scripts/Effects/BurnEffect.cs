﻿using UnityEngine;
using System.Collections;

public class BurnEffect : Effect {

	[SerializeField]
	private float burnDamage, timeToLive;
	
	private bool isBurning = false;
	private float oBurnDamage, oTimeToLive;

	private EnemyBehaviour target;
	private EnemyStatusManager targetStatus;
	private EnemyMovementController targetMovement;
	private EnemyActionController targetAction;
	private Coroutine removeRoutine;
	
	void Awake () {
		oBurnDamage = burnDamage;
		oTimeToLive = timeToLive;
	}
	
	new void Start () {
		base.Start();
	}
	
	void FixedUpdate () {
		if (isBurning) {
			target.GetHit(burnDamage, 2);
		}
	}
	
	void ResetTarget () {
		targetMovement.UnPanic();
		targetAction.EnableAct();
		targetStatus.Unregister();
	}
	
	protected override void ResetToDefault () {
		timeToLive = oTimeToLive;
		burnDamage = oBurnDamage;
		isBurning = false;
		target = null;
		targetStatus = null;
		targetMovement = null;
		targetAction = null;
		removeRoutine = null;	
	}
	
	protected override void StopEffect (bool isNaturalRemove) {
		if (!isNaturalRemove) {
			StopCoroutine(removeRoutine);
		}
		ResetTarget();
		ResetToDefault();
		ReturnToPool();
	}
	
	public override void StartEffect () {
		target = gameObject.GetComponentInParent<EnemyBehaviour>();
		targetStatus = target.StatusManager;
		targetMovement = target.MovementController;
		targetAction = target.ActionController;
		
		burnDamage *= Time.fixedDeltaTime;
		isBurning = true;
		
		targetMovement.Panic();
		targetAction.DisableAct();
		
		removeRoutine = StartCoroutine(Remove());
	}
	
	IEnumerator Remove () {
		yield return new WaitForSeconds(timeToLive);
		StopEffect(true);
	}
	
}

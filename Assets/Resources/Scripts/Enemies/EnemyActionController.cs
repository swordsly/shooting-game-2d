﻿using UnityEngine;
using System.Collections;

public class EnemyActionController : MonoBehaviour {

	public enum TargetTypes {
		Castle, Unit, None
	};

	private float actCD, oActSpeed, attack;
	
	bool _isWithinAttackRange, _canAct;
	
	MonoBehaviour _target;
	TargetTypes _targetType;
	
	private CastleBehaviour targetCastle;
	
	void OnEnable () {
		actCD = oActSpeed;
		_isWithinAttackRange = false;
		_canAct = true;
	}
	
	void FixedUpdate () {
		if (_isWithinAttackRange && _canAct) {
			actCD -= Time.fixedDeltaTime;
			if (actCD <= 0f) {
				Attack();
				actCD = oActSpeed;
			}
		}
		
	}
	
	void Attack () {
		switch (_targetType) {
			case TargetTypes.Castle:
				CastleBehaviour toTarget = (CastleBehaviour) _target;
				toTarget.GetHit(attack);
				break;
			case TargetTypes.Unit:
				break;
		}
	}
	
	public void SetTarget (MonoBehaviour target, TargetTypes type) {
		_targetType = type;
		_target = target;
		_isWithinAttackRange = true;
	}
	
	public void StopAttack () {
		_targetType = TargetTypes.None;
		_target = null;
		_isWithinAttackRange = false;
	}
	
	public void EnableAct () {
		_canAct = true;
	}
	
	public void DisableAct () {
		_canAct = false;
	}
	
	public void SetActSpeed (float aSpd) {
		oActSpeed = aSpd;
		actCD = oActSpeed;
	}
	
	public void SetAttack (float a) {
		attack = a;
	}
	
}

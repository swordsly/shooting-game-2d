﻿using UnityEngine;
using System.Collections;

public class ChillEffect : MonoBehaviour {

	[SerializeField]
	private float m_reduceMove, m_reduceAct, m_timeToLive;
	
	private EnemyBehaviour m_enemyScript;
	private float oMS, oAS;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void ActivateChill () {
		m_enemyScript = gameObject.GetComponentInParent<EnemyBehaviour>();
		if (m_enemyScript.IsBurned) {
			Destroy(gameObject.transform.parent.FindChild("fireTip").gameObject);
			m_enemyScript.IsBurned = false;
		}
		m_enemyScript.IsChilled = true;
		oMS = m_enemyScript.MoveSpeed;
		oAS = m_enemyScript.ActSpeed;
		m_enemyScript.MoveSpeed *= m_reduceMove;
		m_enemyScript.ActSpeed *= m_reduceAct;
		StartCoroutine(Remove());
	}
	
	public void ResetTargetStats () {
		m_enemyScript.MoveSpeed = oMS;
		m_enemyScript.ActSpeed = oAS;
	}
	
	IEnumerator Remove () {
		yield return new WaitForSeconds(m_timeToLive);
		ResetTargetStats();
		m_enemyScript.IsChilled = false;
		Destroy(gameObject);
	}
}

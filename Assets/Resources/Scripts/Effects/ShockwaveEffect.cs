﻿using UnityEngine;
using System.Collections;

public class ShockwaveEffect : Effect {

	public float maxRadius;
	public float damage;
	public float minKnockBack;
	public float maxKnockBack;
	
	private bool isExploding = false;
	private float currRadius;
	
	void OnEnable () {
		currRadius = gameObject.transform.localScale.x;
	}
	
	new void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	void Update () {
		if (isExploding) {
			currRadius += 1.5f;
			if (currRadius < maxRadius) {
				gameObject.transform.localScale = new Vector3(currRadius, currRadius, 1f);
			} else {
				StartCoroutine(Remove());
			}
		}
	}	
	
	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == "Enemy") {
			EnemyBehaviour enemy = other.attachedRigidbody.gameObject.GetComponent<EnemyBehaviour>();
			Vector2 origin = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
			Vector2 target = new Vector2(other.gameObject.transform.position.x, other.gameObject.transform.position.y);
			float difference = Vector2.Distance(origin, target);
			enemy.MovementController.KnockAway(Random.Range(minKnockBack, maxKnockBack), gameObject.transform.localPosition, 
											   damage/Mathf.Max(1, Mathf.RoundToInt(difference)));
			enemy.GetHit(damage, 1);
		}
	}
	
	protected override void ResetToDefault () {
		isExploding = false;
		gameObject.GetComponent<CircleCollider2D>().enabled = false;
		gameObject.GetComponent<SpriteRenderer>().enabled = false;
		gameObject.transform.localScale = Vector3.one;
	}
	
	protected override void StopEffect (bool isNaturalRemove) {
		ResetToDefault();
		ReturnToPool();
	}
	
	public override void StartEffect () {
		isExploding = true;
		gameObject.GetComponent<CircleCollider2D>().enabled = true;
		gameObject.GetComponent<SpriteRenderer>().enabled = true;
	}
	
	IEnumerator Remove () {
		yield return new WaitForSeconds(0.1f);
		StopEffect(true);
	}
}

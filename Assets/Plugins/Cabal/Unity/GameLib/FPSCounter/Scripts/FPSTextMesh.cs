﻿using UnityEngine;

namespace Cabal.Unity
{
    public class FPSTextMesh : FPSTextBase
    {
        public TextMesh textMesh;

        protected override void OnFPSChanged(float fps)
        {
            base.OnFPSChanged(fps);
            textMesh.text = fpsText;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	
	[System.Serializable]
	public struct Arrows {
		public GameObject normal;
		public GameObject bomb;
		public GameObject fire;
		public GameObject gas;
		public GameObject electric;
		public GameObject blackhole;
	}
	
	[System.Serializable]
	public struct Effects {
		public GameObject explosion;
		public GameObject burn;
		public GameObject gas;
		public GameObject stun;
		public GameObject blackhole;
	}
	
	[System.Serializable]
	public struct Enemies {
		public GameObject groundType;
		public GameObject flyingType;
		public GameObject boss;
	}
	
	public static int shotsFired, shotsMissed, enemyCount = -1;
	public static string victory;
	
	public GameObject poolManager, enemyFactory, arrowManager;
	public GameObject ground;
	public GameObject castle;
	public GameObject hero;
	public GameObject bow;
	public GameObject currArrow, currEffect;
	
	public Text arrowText, enemyCountText, clockText;
	
	public bool hasNextRound;
	
	public GameObject roundObject;
	public int totalEnemy;
	
	public Arrows allArrows = new Arrows();
	public Effects allEffects = new Effects();
	public Enemies allEnemies = new Enemies();
	
	private bool isGameOver = false, isBossSpawned = false, isPaused;
	private bool sizeMode, spamMode;
	
	private string arrowType = string.Empty;

	private PoolManager pmScript;
	private EnemyFactoryManager efmScript;
	private BowBehaviour bowScript;
	
	void Awake () {
		shotsFired = 0;
		shotsMissed = 0;
		enemyCount = 0;
		victory = string.Empty;
	}

	void Start () {
		pmScript = poolManager.GetComponent<PoolManager>();
//		efmScript = enemyFactory.GetComponent<EnemyFactoryManager>();
		
		bowScript = bow.GetComponent<BowBehaviour>();
		currArrow = allArrows.normal;
		currEffect = null;
		arrowText.text = "Normal";
		
		sizeMode = MenuManager.sizeMode;
		spamMode = MenuManager.spamMode;
		
		isPaused = false;
		
		if (sizeMode || spamMode) {
			arrowManager.SetActive(false);
		}
		
		enemyCount = totalEnemy;
	}
	
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			Application.LoadLevel("menu");
		}
//		if (efmScript.IsBossAlive() && enemyCount == 0) {
//			if (hasNextRound) {
//				Application.LoadLevel(Application.loadedLevel + 1);
//			} else {
//				victory = "YAY YOU WON!";
//				GameOver();
//			}
//		}
		if (enemyCount == 0) {
			if (hasNextRound) {
				Application.LoadLevel(Application.loadedLevel + 1);
			} else {
				victory = "YAY YOU WON!";
				GameOver();
			}
		}
	}
	
	void FixedUpdate () {
//		enemyCount = efmScript.EnemyCount;
		enemyCountText.text = enemyCount.ToString();
		clockText.text = Mathf.Round(Time.timeSinceLevelLoad).ToString();
	}
	
	public void SceneSetup () {
		bowScript.enabled = true;
//		bowScript.SetArrowType(currArrow, currEffect);
		
//		efmScript.StartSpawning();
		roundObject.SetActive(true);
	}
	
	public void GameOver () {
		isGameOver = true;
		Application.LoadLevel("gameover");
	}
	
	public void Pause () {
		isPaused = !isPaused;
		if (isPaused) {
			Time.timeScale = 0f;
		} else {
			Time.timeScale = 1f;
		}
	}
	
	public void NormalArrowNow () {
		arrowText.text = "Normal";
		currArrow = allArrows.normal;
		currEffect = null;
		bowScript.SetArrowType(currArrow, currEffect);
	}
	
	public void FireArrowNow () {
		arrowText.text = "Fire";
		currArrow = allArrows.fire;
		currEffect = allEffects.burn;
		bowScript.SetArrowType(currArrow, currEffect);
	}
	
	public void GasArrowNow () {
		arrowText.text = "Gas";
		currArrow = allArrows.gas;
		currEffect = allEffects.gas;
		bowScript.SetArrowType(currArrow, currEffect);
	}
	
	public void ElectricArrowNow () {
		arrowText.text = "Electric";
		currArrow = allArrows.electric;
		currEffect = allEffects.stun;
		bowScript.SetArrowType(currArrow, currEffect);
	}
	
	public void BombArrowNow () {
		arrowText.text = "Bomb";
		currArrow = allArrows.bomb;
		currEffect = allEffects.explosion;
		bowScript.SetArrowType(currArrow, currEffect);
	}
	
	public void BlackholeArrowNow () {
		arrowText.text = "Blackhole";
		currArrow = allArrows.blackhole;
		currEffect = allEffects.blackhole;
		bowScript.SetArrowType(currArrow, currEffect);
	}
	
	public static int GetTotalShots () {
		return shotsFired;
	}
	
	public static void UpdateTotalShots () {
		shotsFired++;
	}
	
	public static int GetMissedShots () {
		return shotsMissed;
	}
	
	public static void UpdateMissedShots () {
		shotsMissed++;
	}
	
	public static void ReduceEnemyCount () {
		enemyCount--;
	}
}

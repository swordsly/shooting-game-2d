﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool : MonoBehaviour {
	
	[System.Serializable]
	public struct Info {
		public GameObject prefab;
		public int maximum;
		public int perBatch;
	}

	[SerializeField]
	private Info info;
	
	private Queue<GameObject> items = new Queue<GameObject>();
	private Coroutine creationRoutine;
	private int count = 0;
	
	// Use this for initialization
	void Start () {
		if (info.prefab != null) {
			creationRoutine = StartCoroutine(CreateRoutine());
		}
	}
	
	GameObject Create (int counter) {
		GameObject item = (GameObject) Instantiate(info.prefab);
		item.name = info.prefab.name + "_" + counter;
		item.transform.parent = gameObject.transform;
		item.SetActive(false);
		return item;
	}
	
	public GameObject GiveItem () {
		return items.Dequeue();
	}
	
	public void KeepItem (GameObject loanedItem) {
		loanedItem.transform.SetParent(gameObject.transform);
		loanedItem.transform.localPosition = Vector3.zero;
		loanedItem.transform.localEulerAngles = Vector3.zero;
		loanedItem.transform.localScale = Vector3.one;
		loanedItem.SetActive(false);
		items.Enqueue(loanedItem);
	}
	
	public void InitializePool (GameObject newPrefab) {
		info.prefab = newPrefab;
		creationRoutine = StartCoroutine(CreateRoutine());
	}
	
	public string GetPrefabName () {
		return info.prefab.name;
	}
	
	IEnumerator CreateRoutine () {
		while (count < info.maximum) {
			for (int i=0; i<info.perBatch; i++) {
				items.Enqueue(Create(count));
				count++;
			}
			yield return new WaitForSeconds(Time.deltaTime);
		}
		gameObject.GetComponentInParent<PoolManager>().IncreaseDone();
		StopCoroutine(creationRoutine);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GasEffect : Effect {
	
	enum States {
		Normal, Combust
	}
	
	[SerializeField]
	private float timeToLive, speedReduction, combustDamage;
	[SerializeField]
	private GameObject slowPrefab, burnPrefab;
	
	private float currTime = 0f;
	private bool startNow = false;
	private States currState;
	
	void OnEnable () {
		currTime = timeToLive;
	}
	
	new void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	void Update () {
		if (startNow) {
			currTime -= Time.deltaTime;
			if (currTime <= 0f) {
				StopEffect();
			}
		}
	}
	
	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == "Enemy" && other.gameObject.GetComponent<EnemyStatusManager>().GetBurnStatus()) {
			Combust();
		}
	}
	
	void OnTriggerStay2D (Collider2D other) {
		if (other.gameObject.tag == "Enemy") {
			switch (currState) {
				case States.Normal:
					if (other.gameObject.GetComponent<EnemyStatusManager>().GetBurnStatus()) {
						Combust();
					} else if (!other.gameObject.GetComponent<EnemyStatusManager>().GetSlowStatus()) {
						Debug.Log(other.gameObject.name);
						GameObject slowObj = pmScript.RequestItem(slowPrefab);
						slowObj.GetComponent<SlowEffect>().Setup(speedReduction, currTime);
						other.gameObject.GetComponent<EnemyStatusManager>().Inflict(slowObj, EnemyStatusManager.Statuses.Slow);
					}
					break;
				case States.Combust:
					if (!other.gameObject.GetComponent<EnemyStatusManager>().GetBurnStatus()) {
						other.gameObject.GetComponent<EnemyStatusManager>().Inflict(pmScript.RequestItem(burnPrefab), EnemyStatusManager.Statuses.Burn);	
					}
					break;
			}
		}
	}
	
	void OnTriggerExit2D (Collider2D other) {
		if (other.gameObject.tag == "Enemy") {
			if (currState == States.Normal && other.gameObject.GetComponent<EnemyStatusManager>().GetSlowStatus()) {
				other.gameObject.GetComponentInChildren<SlowEffect>().StopEffect();	
			}
		}
	}
	
	protected override void ResetToDefault () {
		currTime = 0f;
		startNow = false;
		currState = States.Normal;
		gameObject.GetComponent<SpriteRenderer>().enabled = false;
		gameObject.GetComponent<PolygonCollider2D>().enabled = false;
		gameObject.transform.GetChild(0).gameObject.SetActive(false);
	}
	
	protected override void StopEffect (bool isNaturalRemove) {
		ResetToDefault();
		ReturnToPool();
	}
	
	public override void StartEffect () {
		gameObject.GetComponent<SpriteRenderer>().enabled = true;
		gameObject.GetComponent<PolygonCollider2D>().enabled = true;
		startNow = true;
	}
	
	public void Combust () {
		if (currState != States.Combust) {
			currState = States.Combust;
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
			gameObject.transform.GetChild(0).gameObject.SetActive(true);
			StartCoroutine(Remove(0.2f));
		}
	}
	
	IEnumerator Remove (float time) {
		yield return new WaitForSeconds(time);
		StopEffect();
	}
}

﻿using UnityEngine;
using System.Collections;

public class PopupBehaviour : MonoBehaviour {

	public string prefabName;

	private GameObject levelObject;
	
	private PoolManager pmScript;

	void Start () {
//		levelObject = GameObject.Find("Level");
		pmScript = GameObject.Find("PoolManager").GetComponent<PoolManager>();
	}

	void OnEnable () {
//		gameObject.transform.SetParent(levelObject.transform);
	}
	
	void FixedUpdate () {
		gameObject.transform.position += Vector3.up * 2f * Time.fixedDeltaTime;
	}
	
	public void Setup (GameObject target) {
		gameObject.transform.position = target.transform.position;
		StartCoroutine(Remove());
	}
	
	IEnumerator Remove () {
		yield return new WaitForSeconds(1f);
		pmScript.ReturnItem(prefabName, gameObject);
	}
	
}

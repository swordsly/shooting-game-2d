﻿using UnityEngine;
using System.Collections;

public class EnemyStatusManager : MonoBehaviour {

	public enum Statuses {
		None, Burn, Slow, Stun, Suck
	}
	
	public GameObject burningSprite;
	
	// Status flags
	private bool isBurned = false;
	private bool isStunned = false;
	private bool isSlowed = false;
	private bool isSucked = false;
	
	private Statuses currStatus = Statuses.None;
	private EnemyBehaviour selfBehaviour;
	private GameObject statusChannel;
	
	void Start () {
		selfBehaviour = gameObject.GetComponent<EnemyBehaviour>();
//		burningSprite = gameObject.transform.FindChild("burning").gameObject;
	}
	
	public void Inflict (GameObject effectObj, Statuses status) {
		// occupied
		if (statusChannel != null) {
			switch (status) {
				case Statuses.Burn:
					if (!isBurned) {
						if (isSlowed || isStunned) {
							statusChannel.GetComponent<Effect>().StopEffect();
						}
						currStatus = status;
						statusChannel = effectObj;
						effectObj.transform.SetParent(gameObject.transform);
						effectObj.SetActive(true);
						effectObj.GetComponent<BurnEffect>().StartEffect();
						burningSprite.SetActive(true);
						isBurned = true;
					} else {
						effectObj.GetComponent<Effect>().ReturnToPool();
					}
					break;
				case Statuses.Slow:
					if (!isBurned && !isStunned && !isSlowed) {
						currStatus = status;
						statusChannel = effectObj;
						effectObj.transform.SetParent(gameObject.transform);
						effectObj.SetActive(true);
						effectObj.GetComponent<SlowEffect>().StartEffect();
						isSlowed = true;
					} else {
						effectObj.GetComponent<Effect>().ReturnToPool();
					}
					break;
				case Statuses.Stun:
					if (!isStunned) {
						if (isSlowed || isBurned) {
							statusChannel.GetComponent<Effect>().StopEffect();
						}
						currStatus = status;
						statusChannel = effectObj;
						effectObj.transform.SetParent(gameObject.transform);
						effectObj.SetActive(true);
						effectObj.GetComponent<StunEffect>().StartEffect();
						isStunned = true;
					} else {
						effectObj.GetComponent<Effect>().ReturnToPool();
					}
					break;
				case Statuses.Suck:
					if (!isSucked) {
						if (isSlowed || isStunned || isBurned) {
							statusChannel.GetComponent<Effect>().StopEffect();
							burningSprite.SetActive(false);
						}
						currStatus = status;
						statusChannel = effectObj;
						effectObj.transform.SetParent(gameObject.transform);
						effectObj.SetActive(true);
						effectObj.GetComponent<SuckEffect>().StartEffect();
						isSucked = true;
					} else {
						effectObj.GetComponent<Effect>().ReturnToPool();
					}
					break;
			default:
					break;
			}
		// not occupied
		} else {
			Debug.Log ("New: " + gameObject.name);
			// register effect
			currStatus = status;
			statusChannel = effectObj;
			effectObj.transform.SetParent(gameObject.transform);
			effectObj.transform.localPosition = Vector3.zero;
			effectObj.SetActive(true);
			// apply effect
			switch (status) {
				case Statuses.Burn:
					effectObj.GetComponent<BurnEffect>().StartEffect();
					burningSprite.SetActive(true);
					isBurned = true;
					break;
				case Statuses.Stun:
					effectObj.GetComponent<StunEffect>().StartEffect();
					isStunned = true;
					break;
				case Statuses.Slow:
					effectObj.GetComponent<SlowEffect>().StartEffect();
					isSlowed = true;
					break;	
				case Statuses.Suck:
					effectObj.GetComponent<SuckEffect>().StartEffect();
					isSucked = true;
					break;
			}
			
		}
	}
	
	public void Unregister () {
		statusChannel = null;
		isBurned = false;
		isStunned = false;
		isSlowed = false;
		isSucked = false;
		burningSprite.SetActive(false);
		currStatus = Statuses.None;
	}
	
	public bool GetBurnStatus () {
		return isBurned;
	}
	
	public bool GetStunStatus () {
		return isStunned;
	}
	
	public bool GetSlowStatus () {
		return isSlowed;
	}
	
	public bool GetSuckStatus () {
		return isSucked;
	}
	
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CastleBehaviour : MonoBehaviour {

	public float maxHp;
	
	public Slider hpBar;

	private Vector3 screenPos;
	private Animator animator;
	private GameManager gameScript;
	private float hp;

	// Use this for initialization
	void Start () {
		hp = maxHp;
		
		animator = gameObject.GetComponent<Animator>();
		
		gameScript = GameObject.Find("SceneManager").GetComponent<GameManager>();
	}
	
	public void GetHit (float damage) {
		hp -= damage;
		hpBar.value = hp / maxHp;
		animator.SetTrigger("Hit");
		if (hp <= 0f) {
			GameManager.victory = "NUUUUU YOU LOST!";
			gameScript.GameOver();
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public abstract class Effect : MonoBehaviour {

	[SerializeField]
	protected string prefabName;

	protected PoolManager pmScript;
	
	protected virtual void Start () {
		pmScript = GameObject.Find("PoolManager").GetComponent<PoolManager>();
	}
	
	protected abstract void ResetToDefault ();
	
	protected abstract void StopEffect (bool boolean);
	
	public void ReturnToPool () {
		pmScript.ReturnItem(prefabName, gameObject);
	}
	
	public abstract void StartEffect ();
	
	public virtual void StopEffect () {
		StopEffect(false);
	}
	
}

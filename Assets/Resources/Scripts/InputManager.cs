﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

	public Vector2 dragDelta = Vector2.zero;
	public float dragAngle = 0f;
	public GameObject joystick, touchpad, touchButton;
	public float threshold;
	public GameObject bow;
	
	private GameManager gm;
	private CNJoystick controller;
	private CNTouchpad tapper;
	private BowBehaviour bowScript;
	private Touch touchT;
	private float conX, conY;
	private bool gotTap = false, release = false;

	// Use this for initialization
	void Start () {
//		gm = this.GetComponentInParent<GameManager>();
		controller = joystick.GetComponent<CNJoystick>();
		bowScript = bow.GetComponent<BowBehaviour>();
		gotTap = MenuManager.enableTap;
		if (gotTap) {
			touchpad.SetActive(true);
			tapper = touchpad.GetComponent<CNTouchpad>();
		}
	}
	
	// Update is called once per frame
	void Update () {
		float movement = Vector3.Distance(controller.GetStickPosition(), controller.GetBasePosition());
		
		if (controller.lift && release) {
			bowScript.FireArrow();
			controller.lift = false;
			release = false;
		}
		if (controller.move && movement >= threshold) {
			Vector3 velocity = controller.GetStickPosition() - controller.GetBasePosition();
			float angle = Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;
			if ((angle <= -93f && angle >= -180f) || (angle <= 180f && angle >= 109f)) {
//				Debug.Log(angle);
				bowScript.SetDirection(controller.GetBasePosition(), controller.GetStickPosition(), angle);
				controller.TweakRotation(angle);
			}
//			conX = Mathf.Abs(controller.GetAxis("Horizontal"));
//			conY = Mathf.Abs(controller.GetAxis("Vertical"));
			release = true;
		} else {
			bowScript.HideTrajection();
			release = false;
		}
		
//		if (gotTap) {
//			if (tapper.move) {
//				touchButton.SetActive(false);
//			}
//			if (tapper.lift) {
//				touchButton.SetActive(true);
//			}
//			if (bowScript.isAngleSet && tapper.lift) {
//				bowScript.FireArrow();
//				tapper.lift = false;
//			}
//		}
	}
	
	void OnGUI () {
//		GUI.contentColor = Color.black;
//		GUI.skin.label.fontSize = 50;
//		GUI.Label(new Rect(0f, 0f, 300f, 50f), controller.GetAxis("Horizontal").ToString());
//		GUI.Label(new Rect(0f, 50f, 300f, 50f), controller.GetAxis("Vertical").ToString());
	}
}

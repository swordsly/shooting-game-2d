﻿using UnityEngine;
using System.Collections;

public class ChemicalEffect : MonoBehaviour {

	[SerializeField]
	private float m_reduceDef, m_timeToLive;
	
	private EnemyBehaviour m_enemyScript;
	private float oDef;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void ActivateChemical () {
		m_enemyScript = gameObject.GetComponentInParent<EnemyBehaviour>();
		if (m_enemyScript.IsChilled) {
			GameObject iceTip = gameObject.transform.parent.FindChild("iceTip").gameObject;
			iceTip.GetComponent<ChillEffect>().ResetTargetStats();
			Destroy(iceTip);
			m_enemyScript.IsChilled = false;
		}
		if (m_enemyScript.IsBurned) {
			Destroy(gameObject.transform.parent.FindChild("fireTip").gameObject);
			m_enemyScript.IsBurned = false;
		}
		if (m_enemyScript.IsShocked) {
			Destroy(gameObject.transform.parent.FindChild("electricTip").gameObject);
			m_enemyScript.IsShocked = false;
		}
		m_enemyScript.IsChemicaled = true;
		oDef = m_enemyScript.Defense;
		m_enemyScript.Defense *= m_reduceDef;
		StartCoroutine(Remove());	
	}
	
	public void ResetTargetStats () {
		m_enemyScript.Defense = oDef;
	}
	
	IEnumerator Remove () {
		yield return new WaitForSeconds(m_timeToLive);
		ResetTargetStats();
		m_enemyScript.IsChemicaled = false;
		Destroy(gameObject);
	}
}

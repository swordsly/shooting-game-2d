﻿using UnityEngine;
using System.Collections;

public class SlowEffect : Effect {

	private float reduce, timeToLive;
	
	private EnemyBehaviour target;
	private EnemyStatusManager targetStatus;
	private EnemyMovementController targetMovement;
	private Coroutine removeRoutine;

	new void Start () {
		base.Start();
	}
	
	protected override void ResetToDefault () {
		reduce = 0f;
		timeToLive = 0f;
		
		target = null;
		targetStatus = null;
		targetMovement = null;
		
		removeRoutine = null;
	}
	
	protected override void StopEffect (bool isNaturalRemove) {
		if (!isNaturalRemove) {
			StopCoroutine(removeRoutine);
		}
		targetMovement.SetSlow(1f);
		targetStatus.Unregister();
		ResetToDefault();
		ReturnToPool();
	}
	
	public override void StartEffect () {
		target = gameObject.GetComponentInParent<EnemyBehaviour>();
		targetStatus = target.StatusManager;
		targetMovement = target.MovementController;

		targetMovement.SetSlow(reduce);
		
		removeRoutine = StartCoroutine(Remove());
	}
	
	public void Setup (float r, float ttl) {
		reduce = r;
		timeToLive = ttl;
	}
	
	IEnumerator Remove () {
		yield return new WaitForSeconds(timeToLive);
		StopEffect(true);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

	public static bool enableTap = false;
	public static bool sizeMode;
	public static bool spamMode;
	public Image helpImage;
	
	private bool helpOn = false;
	private string tapString = "Tap";
	
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			Application.Quit();
		}
	}

	public void PressStart () {
		Application.LoadLevel(Application.loadedLevel + 1);
	}
	
	public void PressHelp () {
		if (!helpOn) {
			helpImage.enabled = true;
		} else {
			helpImage.enabled = false;
		}
		helpOn = !helpOn;
	}
	
}

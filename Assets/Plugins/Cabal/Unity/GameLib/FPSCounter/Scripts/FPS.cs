using UnityEngine;

namespace Cabal.Unity
{
    public class FPS : MonoBehaviour
    {
        public float updateInterval = 0.5f;

        float _fps;
        public float fps { get { return _fps; } }

        float _previousRealTime = 0.0f;
        float _accumTime = 0.0f;
        int   _frames = 0;

        public event System.Action<float> onFPSChanged;

        void OnEnable()
        {
            RecalibrateTime();
        }

        void Update()
        {
            float currentTime = Time.realtimeSinceStartup;
            float deltaTime = currentTime - _previousRealTime;
            _accumTime += deltaTime; //(deltaTime > 0.0f) ? (Time.timeScale / deltaTime) : 0.0f;
            ++_frames;

            if (_accumTime >= updateInterval)
            {
                _fps = _frames / _accumTime;

                _accumTime = updateInterval - _accumTime;
                _frames = 0;

                if (onFPSChanged != null)
                {
                    onFPSChanged(_fps);
                }
            }
            _previousRealTime = currentTime;
        }

        void OnApplicationFocus(bool focus)
        {
            if (focus)
                RecalibrateTime();
        }

        void RecalibrateTime()
        {
            _previousRealTime = Time.realtimeSinceStartup;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour {

	private Dictionary<string, GameObject> poolTable = new Dictionary<string, GameObject>();
	private int totalDone = 0;
	private int children;

	void Awake () {
		children = gameObject.transform.childCount;
		for (int i=0; i<children; i++) {
			GameObject childPool = gameObject.transform.GetChild(i).gameObject;
			poolTable.Add(childPool.GetComponent<Pool>().GetPrefabName(), childPool);
		}
	}
	
	// Returns allocated object requested by Client
	public GameObject RequestItem (GameObject prefab) {
		// check prefab in table
		if (prefab == null) {
			return null;
		}
		
		if (poolTable.ContainsKey(prefab.name)) {
			// call pool to return requested item
			GameObject pool = poolTable[prefab.name];
			return pool.GetComponent<Pool>().GiveItem();
		} else {
			GameObject newPool = (GameObject) Instantiate(Resources.Load("Objects/Pool"));
			newPool.GetComponent<Pool>().InitializePool(prefab);
			return newPool.GetComponent<Pool>().GiveItem();
		}
	}
	
	// Sends returned object from Client to Pool for storage
	public void ReturnItem (string prefabName, GameObject loanedItem) {
		Debug.Log(prefabName + " || " + loanedItem.ToString());
		if (poolTable.ContainsKey(prefabName)) {
			GameObject pool = poolTable[prefabName];
			pool.GetComponent<Pool>().KeepItem(loanedItem);
		}
	}
	
	public void IncreaseDone () {
		totalDone++;
		if (totalDone >= children) {
			GameObject.Find("SceneManager").GetComponent<GameManager>().SceneSetup();
		}
	}
}

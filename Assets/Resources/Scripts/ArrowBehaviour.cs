﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrowBehaviour : MonoBehaviour {

	enum Tips {
		Physical, Fire, Ice, Electric, Chemical, Explosive, Gas, Blackhole, Nothing
	};
	
	[SerializeField]
	private Tips m_type;
	[SerializeField]
	private float m_damage;
	[SerializeField]
	private string type;
	public float reloadTime;

	private GameObject effectObject;
	private Rigidbody2D m_rb;
	private PoolManager pmScript;
	
	void Start () {
		m_rb = gameObject.GetComponent<Rigidbody2D>();
		pmScript = GameObject.Find("PoolManager").GetComponent<PoolManager>();
	}
	
	void OnEnable () {
		if (m_type != Tips.Physical) {
			effectObject = gameObject.GetComponentInChildren<Effect>().gameObject;
		}
	}
	
	void FixedUpdate () {
		if (m_rb.gravityScale > 0f) {
			gameObject.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(m_rb.velocity.y - (Physics2D.gravity.magnitude * 4.1f) * Time.fixedDeltaTime, m_rb.velocity.x) * Mathf.Rad2Deg);
		}
	}
	
	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == "Outzone") {
			StopNow();
			RemoveNow();
		} else {
			switch (m_type) {	
				case Tips.Physical:
					if (other.gameObject.tag == "Enemy") {
						other.attachedRigidbody.gameObject.GetComponent<EnemyBehaviour>().GetHit(m_damage, other.gameObject.name);
						StopNow();
						RemoveNow();
					} else if (other.gameObject.tag == "Ground") {
						StartCoroutine(Remove());
					}
					break;
				case Tips.Fire:
					if (other.gameObject.tag == "Enemy") {
						other.attachedRigidbody.gameObject.GetComponent<EnemyBehaviour>().GetHit(m_damage, other.gameObject.name);
						other.attachedRigidbody.gameObject.GetComponent<EnemyStatusManager>().Inflict(effectObject, EnemyStatusManager.Statuses.Burn);
						effectObject = null;
						StopNow();
						RemoveNow();
					} else if (other.gameObject.tag == "Gas") {
						other.gameObject.GetComponent<GasEffect>().Combust();
						StopNow();
						RemoveNow();
					} else if (other.gameObject.tag == "Ground") {	
						StartCoroutine(Remove());
					}
					break;
				case Tips.Explosive:
					if (other.gameObject.tag == "Ground") {
						Transform target = other.gameObject.transform.parent;
						effectObject.transform.SetParent(target);
						effectObject.GetComponent<ShockwaveEffect>().StartEffect();	
						StopNow();
						RemoveNow();
					}
					break;
				case Tips.Gas:
					if (other.gameObject.tag == "Ground") {
						Transform target = other.gameObject.transform.parent;
						effectObject.transform.SetParent(target);
						effectObject.transform.localEulerAngles = Vector3.zero;
						effectObject.transform.localPosition = Vector3.right * effectObject.transform.localPosition.x + Vector3.up * 14f;
						effectObject.GetComponent<GasEffect>().StartEffect();
						StopNow();
						RemoveNow();
					}
					break;
				case Tips.Electric:
					if (other.gameObject.tag == "Enemy") {
						other.gameObject.GetComponent<EnemyBehaviour>().GetHit(m_damage, 1);
						other.gameObject.GetComponent<EnemyStatusManager>().Inflict(effectObject, EnemyStatusManager.Statuses.Stun);
						effectObject = null;
						StopNow();
						RemoveNow();
					} else if (other.gameObject.tag == "Ground") {
						StartCoroutine(Remove());
					}
					break;
				case Tips.Blackhole:
					if (other.gameObject.tag == "Ground") {
						Transform target = other.gameObject.transform.parent;
						effectObject.transform.SetParent(target);
						effectObject.transform.localEulerAngles = Vector3.zero;
						effectObject.transform.localPosition = Vector3.right * effectObject.transform.localPosition.x + Vector3.up * 12.5f;
						effectObject.GetComponent<BlackholeEffect>().StartEffect();	
						StopNow();
						RemoveNow();
					}
					break;
				default:
					break;
			}
		}		
	}
	
	void StopNow () {
		m_rb.velocity = Vector3.zero;
		m_rb.gravityScale = 0f;
		gameObject.GetComponent<BoxCollider2D>().enabled = false;
	}
	
	void RemoveNow () {
		if (effectObject != null) {
			if (effectObject.transform.IsChildOf(gameObject.transform)) {
				effectObject.GetComponent<Effect>().ReturnToPool();
			}
		}
		gameObject.GetComponent<BoxCollider2D>().enabled = true;
		effectObject = null;
		pmScript.ReturnItem(type, gameObject);
	}
	
	public void Activate (Vector3 force) {
		m_rb.gravityScale = 4.1f;
		m_rb.AddForce(new Vector2(force.x, force.y), ForceMode2D.Impulse);
		gameObject.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(m_rb.velocity.y - (Physics2D.gravity.magnitude * 4.1f) * Time.fixedDeltaTime, m_rb.velocity.x) * Mathf.Rad2Deg);
	}
	
	IEnumerator Remove () {
		StopNow();
		yield return new WaitForSeconds(2f);
//		GameManager.UpdateMissedShots();
		RemoveNow();
	}
}

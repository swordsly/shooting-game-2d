﻿using UnityEngine;
using System.Collections;

public class SuckEffect : Effect {
	
	private float maxDistance, threshold = 0.1f, timeToLive, point, damage;
	private bool isRunning = false;
	
	private Vector3 targetPosition, sourcePosition;
	
	private EnemyBehaviour target;
	private EnemyStatusManager targetStatus;
	private EnemyMovementController targetMovement;
	private EnemyActionController targetAction;
	private BlackholeEffect blackhole;
	private Coroutine removeRoutine;

	new void Start () {
		base.Start();
	}
	
	void FixedUpdate () {
		if (isRunning) {
			Suck();
			target.GetHit(damage, 1);
		}
	}
	
	void Suck () {
		float distanceGap = Mathf.Abs(targetPosition.x - sourcePosition.x);
		if (distanceGap > threshold) {
			point += blackhole.GetPullStrength(point);
			Vector3 movement = Vector3.Lerp(targetPosition, sourcePosition, point);
			target.gameObject.transform.position = movement;
		}
	}
	
	protected override void ResetToDefault () {
		target = null;
		targetStatus = null;
		targetMovement = null;
		targetAction = null;
		blackhole = null;
		removeRoutine = null;
		
		targetPosition = Vector3.zero;
		sourcePosition = Vector3.zero;
		
		maxDistance = 0f;
		point = 0f;
		timeToLive = 0f;
		damage = 0f;
		
		isRunning = false;
	}
	
	protected override void StopEffect (bool isNaturalRemove) {
		if (!isNaturalRemove) {
			StopCoroutine(removeRoutine);
		}
		target.gameObject.transform.localPosition += Vector3.up * (targetPosition.y - target.gameObject.transform.position.y);
		targetMovement.enabled = true;
		targetAction.EnableAct();
		targetStatus.Unregister();
		ResetToDefault();
		ReturnToPool();
	}
	
	public override void StartEffect () {
		target = gameObject.GetComponentInParent<EnemyBehaviour>();
		targetStatus = target.StatusManager;
		targetMovement = target.MovementController;
		targetAction = target.ActionController;
		
		targetPosition = target.gameObject.transform.position;
		sourcePosition = blackhole.gameObject.transform.position;
		maxDistance = blackhole.MaxDistance;
		point = 1f - Mathf.Abs(targetPosition.x - sourcePosition.x) / maxDistance;
		
		isRunning = true;
		targetMovement.enabled = false;
		targetAction.DisableAct();
		
		removeRoutine = StartCoroutine(Remove());
	}
	
	public void Setup (BlackholeEffect bhe, float ttl, float d) {
		blackhole = bhe;
		timeToLive = ttl;
		damage = d * Time.fixedDeltaTime;
	}
	
	IEnumerator Remove () {
		yield return new WaitForSeconds(timeToLive);
		StopEffect(true);
	}
	
}

﻿using UnityEngine;
using UnityEngine.UI;

namespace Cabal.Unity
{
    public class FPSText : FPSTextBase
    {
        public Text text;

        protected override void OnFPSChanged(float fps)
        {
            base.OnFPSChanged(fps);
            text.text = fpsText;
        }
    }
}

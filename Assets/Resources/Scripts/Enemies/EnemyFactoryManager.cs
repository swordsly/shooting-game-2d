﻿using UnityEngine;
using System.Collections;

public class EnemyFactoryManager : MonoBehaviour {

	[System.Serializable]
	public struct GroundUnitSettings {
		public int totalWaves;
		public int inGroupsOf;
		public float interval;
	}

	[System.Serializable]
	private struct Enemies {
		public GameObject groundType;
		public GameObject flyingType;
		public GameObject boss;
	}

	[SerializeField]
	private GameObject groundSpawn, airSpawn, bossSpawn, poolManager, sceneManager;
	[SerializeField]
	private GameObject gs07, gs085, gs12, gs135, gs15, gs2, gs25, gs3;
	
	public GroundUnitSettings groundSettings = new GroundUnitSettings();
	
	private GameObject[] gsArr;
	private float[] fArr;
	
	private int enemyCount = 0;
	private bool isDone = false, isBossSpawned = false;
	private bool isSizeMode, isSpamMode;
	
	private GameObject gEnemy, fEnemy, bEnemy;
	private Coroutine gRoutine, fRoutine, bRoutine;
	private PoolManager pmScript;
	private GameManager gmScript;

	// Use this for initialization
	void Start () {
		pmScript = poolManager.GetComponent<PoolManager>();
		gmScript = sceneManager.GetComponent<GameManager>();
		
		gsArr = new GameObject[8] {gs07, gs085, gs12, gs135, gs15, gs2, gs25, gs3};
		fArr = new float[8] {0.7f, 0.85f, 1.2f, 1.35f, 1.5f, 2f, 2.5f, 3f};
		
		isSizeMode = MenuManager.sizeMode;
		isSpamMode = MenuManager.spamMode;
	}
	
	void CreateEnemy () {
		gEnemy = pmScript.RequestItem(gmScript.allEnemies.groundType);
		gEnemy.transform.parent = gameObject.transform;
		
		if (isSizeMode) {
			int i = Random.Range(0, 9);
			if (i < 8) {
				gEnemy.transform.localScale = new Vector3(fArr[i], fArr[i], 1f);
				gEnemy.transform.localPosition = gsArr[i].transform.localPosition;
			} else {
				gEnemy.transform.localPosition = groundSpawn.transform.localPosition;
			}
		} else {
			gEnemy.transform.localPosition = groundSpawn.transform.localPosition;
//			gEnemy.GetComponent<EnemyBehaviour>().SetMaxHp(20f);
		}
		
		gEnemy.SetActive(true);
		enemyCount++;
	}
	
	void CreateFlyingEnemy () {
		fEnemy = pmScript.RequestItem(gmScript.allEnemies.flyingType);
		GameObject exObj = pmScript.RequestItem(gmScript.allEffects.explosion);
		exObj.transform.parent = fEnemy.transform;
		exObj.transform.localPosition = new Vector3(-1.5f, -0.25f, 0f);
		exObj.SetActive(true);
		fEnemy.transform.parent = gameObject.transform;
		fEnemy.transform.position = airSpawn.transform.position;
		fEnemy.SetActive(true);
		enemyCount++;
	}
	
	void CreateBoss () {
		bEnemy = pmScript.RequestItem(gmScript.allEnemies.boss);
		bEnemy.transform.parent = gameObject.transform;
		bEnemy.transform.position = bossSpawn.transform.position;
		if (isSpamMode || !isSizeMode) {
			bEnemy.GetComponent<EnemyBehaviour>().SetMaxHp(100f);
		}
		bEnemy.SetActive(true);
		enemyCount++;
	}
	
	public void StartSpawning () {
		if (!isSpamMode) {
			gRoutine = StartCoroutine(SpawnGround());
		}
//		fRoutine = StartCoroutine(SpawnAir());
//		if (!isSizeMode && !isSpamMode) {
//			bRoutine = StartCoroutine(SpawnBoss());
//		}
//		if (isSpamMode) {
//			CreateBoss();
//			isBossSpawned = true;
//		}
//		CreateEnemy();
	}
	
	public bool IsBossAlive () {
//		if (isBossSpawned && bEnemy == null) {
//			return true;
//		}
//		return false;
		return isDone;
	}
	
	public void UnsetBoss () {
		bEnemy = null;
	}
	
	public void ReduceEnemyCount () {
		enemyCount--;
		Debug.Log(enemyCount);
	}
	
	public int EnemyCount {
		get { return enemyCount; }
	}
	
	IEnumerator SpawnGround () {
		int count = groundSettings.totalWaves;
		while (count > 0) {
			StartCoroutine(SubSpawnGround(groundSettings.inGroupsOf));
			yield return new WaitForSeconds(groundSettings.interval + (groundSettings.inGroupsOf - 1f));
			count--;
		}
		isDone = true;
	}
	
	IEnumerator SubSpawnGround (int count) {
		while (count > 0) {
			CreateEnemy();
			count--;
			yield return new WaitForSeconds(1f);
		}
	}
	
	IEnumerator SpawnAir () {
		while (!isDone) {
			yield return new WaitForSeconds(4f);
			CreateFlyingEnemy();
			yield return new WaitForSeconds(5f);
		}
	}
	
	IEnumerator SpawnBoss () {
		yield return new WaitForSeconds(120f);
		if (gRoutine != null) {
			StopCoroutine(gRoutine);
		}
		if (fRoutine != null) {
			StopCoroutine(fRoutine);
		}
		CreateBoss();
		isBossSpawned = true;
		yield return new WaitForSeconds(0f);
	}
	
}

﻿using UnityEngine;
using System.Collections;

public class StunEffect : Effect {

	[SerializeField]
	private float timeToLive;

	private bool isStunning = false;
	private float oTimeToLive;

	private EnemyBehaviour target;
	private EnemyStatusManager targetStatus;
	private EnemyMovementController targetMovement;
	private Coroutine removeRoutine, stunRoutine;
	private SpriteRenderer parentSprite;

	void Awake () {
		oTimeToLive = timeToLive;
	}

	new void Start () {
		base.Start();
	}
	
	void Stun () {
		parentSprite.color = Color.blue;
		targetMovement.enabled = false;
//		enemyScript.DoStunState();		
	}
	
	void UnStun () {
		parentSprite.color = Color.white;
		targetMovement.enabled = true;
//		target.UndoStunState();
	}
	
	void ResetTarget () {
		UnStun();
		target.IsShocked = false;
		targetMovement.enabled = true;
		targetStatus.Unregister();
	}
	
	protected override void ResetToDefault () {
		timeToLive = oTimeToLive;
		isStunning = false;
		target = null;
		targetStatus = null;
		targetMovement = null;
		removeRoutine = null;
		stunRoutine = null;
		parentSprite = null;
	}
	
	protected override void StopEffect (bool isNaturalRemove) {
		if (!isNaturalRemove) {
			StopCoroutine(removeRoutine);
		}
		StopCoroutine(stunRoutine);
		ResetTarget();
		ResetToDefault();
		ReturnToPool();
	}
	
	public override void StartEffect () {
		target = gameObject.GetComponentInParent<EnemyBehaviour>();
		targetStatus = target.StatusManager;
		targetMovement = target.MovementController;
		parentSprite = gameObject.transform.parent.gameObject.GetComponent<SpriteRenderer>();
		isStunning = true;
		target.IsShocked = true;
		stunRoutine = StartCoroutine(DoStunNow());
		removeRoutine = StartCoroutine(Remove());
	}
	IEnumerator Remove () {
		yield return new WaitForSeconds(timeToLive);
		StopEffect(true);
	}
	
	IEnumerator DoStunNow () {
		while (isStunning) {
			Stun();
			yield return new WaitForSeconds(1f);
			UnStun();
			yield return new WaitForSeconds(1f);
		}
	}
}

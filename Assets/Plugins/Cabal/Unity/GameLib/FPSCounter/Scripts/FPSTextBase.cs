﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

namespace Cabal.Unity
{
    public abstract class FPSTextBase : MonoBehaviour
    {
        [SerializeField]
        protected FPS _fps;

        string _fpsText;
        public string fpsText { get { return _fpsText; } }

        StringBuilder _stringBuilder = new StringBuilder(128);

        void OnEnable()
        {
            _fps.onFPSChanged += OnFPSChanged;
        }

        void OnDisable()
        {
            _fps.onFPSChanged -= OnFPSChanged;
        }

        protected virtual void OnFPSChanged(float fps)
        {
            // Allocates ~178 B.
//            _fpsText = fps.ToString("n2") + " FPS";

            // Allocates slightly less memory garbage (~156 B).
            _stringBuilder.Length = 0;
            _stringBuilder.Append(fps.ToString("n2"));
            _stringBuilder.Append(" FPS");
            _fpsText = _stringBuilder.ToString();
        }
    }
}

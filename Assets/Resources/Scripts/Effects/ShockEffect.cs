﻿using UnityEngine;
using System.Collections;

public class ShockEffect : MonoBehaviour {

	[SerializeField]
	private float m_timeToLive;
	
	private EnemyBehaviour m_enemyScript;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void ActivateShock () {
		m_enemyScript = gameObject.GetComponentInParent<EnemyBehaviour>();
		m_enemyScript.IsShocked = true;
		StartCoroutine(Remove());
	}
	
	IEnumerator Remove () {
		yield return new WaitForSeconds(m_timeToLive);
		m_enemyScript.IsShocked = false;
		Destroy(gameObject);
	}
}

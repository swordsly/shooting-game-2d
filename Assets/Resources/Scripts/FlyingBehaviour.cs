﻿using UnityEngine;
using System.Collections;

public class FlyingBehaviour : MonoBehaviour {

	[SerializeField]
	private GameObject explosionPrefab;

	private bool isDropping = false;
	private Vector3 screenPos = Vector3.zero;
	private int counter = 1;
	private PoolManager pmScript;
	
	// Use this for initialization
	void Start () {
		pmScript = GameObject.Find("PoolManager").GetComponent<PoolManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void FixedUpdate () {
		if (isDropping) {
			screenPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
			screenPos += Vector3.up * (Physics2D.gravity.y * counter) * Time.fixedDeltaTime;
			gameObject.transform.position = Camera.main.ScreenToWorldPoint(screenPos);
			counter++;
		}
	}
	
	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == "Ground") {
			GameObject exObj = gameObject.GetComponentInChildren<ShockwaveEffect>().gameObject;
			Transform target = other.gameObject.transform;
			exObj.transform.SetParent(target);
			exObj.GetComponent<ShockwaveEffect>().StartEffect();
			Reset();
			pmScript.ReturnItem(gameObject.GetComponent<EnemyBehaviour>().prefabName, gameObject);
		}
	}
	
	void Reset() {
		isDropping = false;
		screenPos = Vector3.zero;
		counter = 1;
		EnemyBehaviour enemyScript = gameObject.GetComponent<EnemyBehaviour>();
		if (enemyScript.IsBurned) {
			BurnEffect burnEffect = gameObject.GetComponentInChildren<BurnEffect>();
			burnEffect.StopEffect();
		} else if (enemyScript.IsShocked) {
			StunEffect stunEffect = gameObject.GetComponentInChildren<StunEffect>();
			stunEffect.StopEffect();
		} else if (enemyScript.IsSlowed) {
			SlowEffect slowEffect = gameObject.GetComponentInChildren<SlowEffect>();
			slowEffect.StopEffect();
		}
		enemyScript.ResetToDefault();
	}
	
	public void DropNow () {
		if (!isDropping) {
			isDropping = true;
//			gameObject.transform.Rotate(Vector3.forward, 90f * gameObject.transform.localScale.x);
			if (gameObject.transform.localScale.x == 1) {
				gameObject.transform.Rotate(Vector3.forward, 90f);
			} else {
				gameObject.transform.Rotate(Vector3.forward, -90f);
			}
		}
	}
	
}

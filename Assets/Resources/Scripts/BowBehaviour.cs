﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BowBehaviour : MonoBehaviour {

	public bool isAngleSet = false;

	[SerializeField]
	private GameObject scenemanager;
	[SerializeField]
	private GameObject poolmanager;
	[SerializeField]
	private GameObject arrowInUse;
	[SerializeField]
	private GameObject effectInUse;
	[SerializeField]
	private GameObject prevArrow;
	[SerializeField]
	private GameObject prevEffect;
	[SerializeField]
	private GameObject currArrow;
	[SerializeField]
	private GameObject currEffect;
	[SerializeField]
	private AnimationCurve powerCurve;
	
	public GameObject defaultArrowManager;
	
	Vector3 outForce;
	bool isLoaded = false;
	float currReloadTime;
	
	string _prevArrowName, _prevEffectName;
	
	LineRenderer line;
	List<GameObject> aimPoints = new List<GameObject>();
	
	PoolManager _pmScript;
	GameManager gmScript;
	ArrowManager _arrowSupplier, _prevArrowSupplier;

	void Awake () {
		gmScript = scenemanager.GetComponent<GameManager>();
		_pmScript = poolmanager.GetComponent<PoolManager>();
		
//		for (int i=0; i<12; i++) {
//			GameObject path = (GameObject) Instantiate(Resources.Load("Objects/aimPath"));
//			path.transform.localPosition = new Vector3(27f, 0f, 0f);
//			aimPoints.Add(path);
//		}
	}

	void OnEnable () {
		line = gameObject.GetComponent<LineRenderer>();
		line.SetWidth(0.02f, 0.02f);
		line.enabled = false;
		
		_arrowSupplier = defaultArrowManager.GetComponent<ArrowManager>();
		_arrowSupplier.OnPressed();
	}
	
	void SetTrajection (Vector3 origin, Vector3 dest, float angle) {
		outForce = -(dest - origin);
//		Debug.Log("o: " + outForce);
		float magnitude = outForce.magnitude;
//		Debug.Log(magnitude);
		outForce.Normalize();
//		Debug.Log(outForce);
		float power = powerCurve.Evaluate(magnitude);
		outForce *= power;
		
		Vector3 finalForce = outForce / arrowInUse.GetComponent<Rigidbody2D>().mass;
//		Debug.Log("f: " + finalForce);
		float velocity = Mathf.Sqrt((finalForce.x * finalForce.x) + (finalForce.y * finalForce.y));
		float fTime = 0f;
		
		fTime += 0.1f;
//		fTime += 0.05f;
		float radAngle = (angle-180f) * Mathf.Deg2Rad;
//		for(int i=0; i<aimPoints.Count; i++) {
		for(int i=0; i<50; i++) {
			float dx = velocity * fTime * Mathf.Cos(radAngle);
			float dy = velocity * fTime * Mathf.Sin(radAngle) - ((Physics2D.gravity.magnitude * 4.1f) * fTime * fTime / 2f);
			Vector3 position = new Vector3(gameObject.transform.position.x + dx, gameObject.transform.position.y + dy, gameObject.transform.position.z);
//			aimPoints[i].transform.position = position;
//			aimPoints[i].transform.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(finalForce.y - (Physics.gravity.magnitude * 4.1f) * fTime, finalForce.x) * Mathf.Rad2Deg);
			line.SetPosition(i, position);
//			fTime += 0.05f;
			fTime += 0.1f;
		}
	}
	
	void ReloadNow () {
		_arrowSupplier.SupplyArrow();
	}
	
	public void UnloadArrow () {
		if (_prevArrowSupplier != null) {
			_prevArrowSupplier.OnReleased();
			if (arrowInUse != null) {
				_pmScript.ReturnItem(_prevArrowName, arrowInUse);
			}
			if (effectInUse != null) {
				_pmScript.ReturnItem(_prevEffectName, effectInUse);
			}
		}
	}
	
	public void UpdateArrowSupplier (ArrowManager am) {
		_prevArrowSupplier = _arrowSupplier;
		_arrowSupplier = am;
	}
	
	public void UpdatePrefabNames (GameObject arrow, GameObject effect) {
		_prevArrowName = arrow.name;
		if (effect != null) {
			_prevEffectName = effect.name;
		}
	}
	
	public void SetDirection (Vector3 origin, Vector3 dest, float angle) {
		line.enabled = true;
		
//		Vector3 velocity = dest - origin;
//		Debug.Log(velocity);

//		float angle = Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;
//		Debug.Log("be: " + angle);
//		if (angle <= 0f && angle >= -180f) {
//			angle = Mathf.Clamp(angle, -180f, -93f);
//		} else {
//			angle = Mathf.Clamp(angle, 110f, 180f);
//		}
//		Debug.Log("af: " + angle);

		if (angle == 0f) {
			gameObject.transform.eulerAngles = Vector3.zero;
		} else {
			Quaternion rotation = Quaternion.Euler(new Vector3(0f, 0f, angle-180f));
			gameObject.transform.rotation = rotation;
			if (arrowInUse != null) {
				arrowInUse.transform.rotation = rotation;
			}
		}
		
		SetTrajection(origin, dest, angle);
	}
	
	public void LoadBow () {
//		if (arrowInUse != null) {
//			pmScript.ReturnItem(prevArrow.name, arrowInUse);
//			if (effectInUse != null) {
//				pmScript.ReturnItem(prevEffect.name, effectInUse);
//			}
//		}
//		arrowInUse = pmScript.RequestItem(currArrow);
//		if (currEffect != null) {
//			effectInUse = pmScript.RequestItem(currEffect);
//			effectInUse.transform.parent = arrowInUse.transform;
//			effectInUse.transform.localPosition = Vector3.right * 0.8f;
//			effectInUse.SetActive(true);
//		}
		if (effectInUse != null) {
			effectInUse.transform.parent = arrowInUse.transform;
			effectInUse.transform.localPosition = Vector3.right * 0.8f;
			effectInUse.SetActive(true);
		}
		arrowInUse.transform.parent = gameObject.transform;
		arrowInUse.transform.localPosition = Vector3.zero;
		arrowInUse.transform.localEulerAngles = Vector3.zero;
		arrowInUse.SetActive(true);
		isLoaded = true;
	}
	
	public void FireArrow () {
		if (isLoaded) {
			arrowInUse.transform.parent = gameObject.transform.parent;
			arrowInUse.GetComponent<ArrowBehaviour>().Activate(outForce);
			arrowInUse = null;
			effectInUse = null;
			isLoaded = false;
			line.enabled = false;
//			for (int i=0; i<aimPoints.Count; i++) {
//				aimPoints[i].transform.localPosition = new Vector3(27f, 0f, 0f);
//				aimPoints[i].transform.localEulerAngles = Vector3.zero;
//			}
			isAngleSet = true;
//			StartCoroutine(Reload());
			ReloadNow();
		}
	}
	
	public void SetArrowType (GameObject arrowPrefab, GameObject effectPrefab) {
//		prevArrow = currArrow;
//		currArrow = arrowPrefab;
//		prevEffect = currEffect;
//		currEffect = effectPrefab;
		arrowInUse = arrowPrefab;
		effectInUse = effectPrefab;
		LoadBow();
	}
	
	public void HideTrajection () {
		if (line != null) {
			line.enabled = false;
		}
	}
	
	IEnumerator Reload () {
		yield return new WaitForSeconds(0.3f);
		LoadBow();
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyBehaviour : MonoBehaviour {
	
	enum Types {
		Ground, Flying
	};
	
	public enum States {
		Moving, Attacking
	};

	[SerializeField]
	private float maxHP, defense, attack, moveSpeed, actSpeed, range;
	[SerializeField]
	private bool knockable;
	[SerializeField]
	private Types m_type;
	[SerializeField]
	private bool isBoss;
	[SerializeField]
	public string prefabName;
	
	public float hp, oMS;
	
	public Slider hpBar;
	
	public GameObject self;
	
	// Controllers, Managers
	[System.Serializable]
	private struct ControllerGroup {
		public EnemyMovementController movement;
		public EnemyActionController action;
		public EnemyStatusManager status;
	}
	
	public GameObject popupPrefab;
	
	[SerializeField]
	private ControllerGroup controllers = new ControllerGroup(); 
	
	// Status flags
	private bool m_isBurned = false;
	private bool m_isChilled = false;
	private bool m_isShocked = false;
	private bool m_isChemicaled = false;
	private bool isSlowed = false;
	
	private Vector3 screenPos = Vector3.zero, startPos, endPos;
	private float actCD;

	States _state = States.Moving;
	public States State {
		get { return _state; }
		set { _state = value; }
	}

	private Animator animator;
	private PoolManager pmScript;
	private EnemyFactoryManager efmScript;

	void Start () {
		hp = maxHP;
		oMS = moveSpeed;
		actCD = actSpeed;
		
		controllers.movement.SetSpeed(moveSpeed);
		controllers.action.SetActSpeed(actSpeed);
		controllers.action.SetAttack(attack);
		
		pmScript = GameObject.Find("PoolManager").GetComponent<PoolManager>();
//		efmScript = GameObject.Find("EnemyFactory").GetComponent<EnemyFactoryManager>();
		animator = self.GetComponent<Animator>();
	}
	
	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == "Castle") {
			controllers.movement.CanMove = false;
			_state = States.Attacking;
			controllers.action.SetTarget(other.gameObject.GetComponent<CastleBehaviour>(), EnemyActionController.TargetTypes.Castle);
		}
	}
	
	void OnTriggerExit2D (Collider2D other) {
		if (other.gameObject.tag == "Castle") {
			controllers.action.StopAttack();
			_state = States.Moving;
			controllers.movement.CanMove = true;
		}
	}
	
	public void ResetToDefault () {
		hp = maxHP;
		moveSpeed = oMS;
		actCD = actSpeed;
		
		hpBar.value = hp;
		
		m_isBurned = false;
		m_isChilled = false;
		m_isShocked = false;
		m_isChemicaled = false;
		isSlowed = false;
	}
	
	public void GetHit (float damage, int type) {
		if (hp > 0f) {
			switch(type) {
			case 1:
				hp -= damage;
				hpBar.value = hp / maxHP;
				break;
			case 2:
				hp -= damage;
				hpBar.value = hp / maxHP;
				break;
			default:
				break;
			}
			animator.SetTrigger("Hit");
		} 
		if (hp <= 0f) {
			hp = 0f;
			switch (m_type) {
			case Types.Flying:
				gameObject.GetComponent<FlyingBehaviour>().DropNow();
				break;
			default:
				if (m_isBurned) {
					BurnEffect burnEffect = gameObject.GetComponentInChildren<BurnEffect>();
					burnEffect.StopEffect();
				} else if (m_isShocked) {
					StunEffect stunEffect = gameObject.GetComponentInChildren<StunEffect>();
					stunEffect.StopEffect();
				} else if (isSlowed) {
					SlowEffect slowEffect = gameObject.GetComponentInChildren<SlowEffect>();
					slowEffect.StopEffect();
				}
				ResetToDefault();
				efmScript.ReduceEnemyCount();
				if (isBoss) {
					efmScript.UnsetBoss();
				}
				pmScript.ReturnItem(prefabName, gameObject);
				break;
			}
		} 
	}
	
	public void GetHit (float damage, string part) {
		if (hp > 0f) {
//			if (part == "Head") {
//				hp = 0f;
//				GameObject headshotText = pmScript.RequestItem(popupPrefab);
//				headshotText.SetActive(true);
//				headshotText.GetComponent<PopupBehaviour>().Setup(gameObject);
//			} else if (part == "Body") {
//				hp -= damage;
//			}
			hp -= damage;
			hpBar.value = hp / maxHP;
			animator.SetTrigger("Hit");
		} 
		if (hp <= 0f) {
			hp = 0f;
			switch (m_type) {
			case Types.Flying:
				gameObject.GetComponent<FlyingBehaviour>().DropNow();
				break;
			default:
				if (m_isBurned) {
					BurnEffect burnEffect = gameObject.GetComponentInChildren<BurnEffect>();
					burnEffect.StopEffect();
				} else if (m_isShocked) {
					StunEffect stunEffect = gameObject.GetComponentInChildren<StunEffect>();
					stunEffect.StopEffect();
				} else if (isSlowed) {
					SlowEffect slowEffect = gameObject.GetComponentInChildren<SlowEffect>();
					slowEffect.StopEffect();
				}
				ResetToDefault();
//				efmScript.ReduceEnemyCount();
				GameManager.ReduceEnemyCount();
				if (isBoss) {
					efmScript.UnsetBoss();
				}
				pmScript.ReturnItem(prefabName, gameObject);
				break;
			}
		} 
	}
	
	public void DoStunState () {
//		m_direction = Vector3.zero;
//		canAct = false;
	}
	
	public void UndoStunState () {
//		m_direction = Vector3.left;
//		gameObject.transform.localScale = Vector3.one;
//		canAct = true;
	}
	
	public void DoSlowState (float reduction) {
		moveSpeed = oMS / reduction;
		isSlowed = true;
	}
	
	public void UndoSlowState (float reduction) {
		moveSpeed = oMS;
		isSlowed = false;
	}
	
	public float ActSpeed {
		get { return actSpeed; }
		set { actSpeed = value; }
	}
	
	public float MoveSpeed {
		get { return moveSpeed; }
		set { moveSpeed = value; }
	}
	
	public float Defense {
		get { return defense; }
		set { defense = value; }
	}
	
	public bool IsBurned {
		get { return m_isBurned; }
		set { m_isBurned = value; }
	}
	
	public bool IsChilled {
		get { return m_isChilled; }
		set { m_isChilled = value; }
	}
	
	public bool IsShocked {
		get { return m_isShocked; }
		set { m_isShocked = value; }
	}
	
	public bool IsChemicaled {
		get { return m_isChemicaled; }
		set { m_isChemicaled = value; }
	}
	
	public bool IsSlowed {
		get { return isSlowed; }
		set { isSlowed = value; }
	}
	
	public void ResetMS () {
		moveSpeed = oMS;
	}
	
	public void SetMaxHp (float m) {
		maxHP = m;
	}
	
	public EnemyMovementController MovementController {
		get { return controllers.movement; }
	}
	
	public EnemyStatusManager StatusManager {
		get { return controllers.status; }
	}
	
	public EnemyActionController ActionController {
		get { return controllers.action; }
	}
}

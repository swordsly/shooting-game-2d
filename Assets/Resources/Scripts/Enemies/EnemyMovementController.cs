﻿using UnityEngine;
using System.Collections;

public class EnemyMovementController : MonoBehaviour {

	public bool knockable;
	public GameObject self;
	
	private float maxBounceHeight, maxBounceTime, bounceTimer = 0f;
	private float oSpeed, cSpeed, leftLimit, slowdown = 1f;
	private bool isPanicking = false, isBouncing = false;
	private bool canBeKnocked = true;
	
	bool _canMove = true;
	public bool CanMove {
		set { _canMove = value; }
	}
	
	private Vector3 direction, oPosition, startPos, endPos;
	
	private Coroutine panicRoutine;

	void Awake () {
		oPosition = gameObject.transform.position;
	}
	
	void OnEnable () {
		cSpeed = oSpeed;
		direction = Vector3.left;
	}
	
	void Start () {
		GameObject stopObj = GameObject.Find("stopPoint");
		leftLimit = stopObj.transform.position.x;
	}
	
	void FixedUpdate () {
		if (isBouncing) {
			Bounce();
		} else {
			Move();
		}
	}

	void Move () {
		if (_canMove) {
			gameObject.transform.position = gameObject.transform.position + (direction * cSpeed * slowdown * Time.fixedDeltaTime);
		}
	}
	
	void Bounce () {
		Vector3 bounce = Vector3.up * Mathf.Sin(Mathf.PI * (bounceTimer / maxBounceTime)) * maxBounceHeight;
		gameObject.transform.position = Vector3.Slerp(startPos, endPos, (bounceTimer / maxBounceTime)) + bounce;
		if (bounceTimer > maxBounceTime) {
			bounceTimer = 0f;
			isBouncing = false;
			gameObject.transform.position = endPos;
		}
		bounceTimer += Time.deltaTime;
	}
	
	public void KnockAway (float distance, Vector3 originLocal, float hit) {
		if (canBeKnocked) {
			canBeKnocked = false;
			if (knockable) {
				Vector3 knockDirection = Vector3.right;
				if (gameObject.transform.localPosition.x < originLocal.x) {
					knockDirection = Vector3.left;
				}
				
				startPos = gameObject.transform.position;
				endPos = startPos + knockDirection * distance;
				if (endPos.x < leftLimit) {
					endPos.x = leftLimit;
				}
				float random = Random.Range(-1, 2) / 10f;
				maxBounceHeight = distance * (1.5f + random);
				maxBounceTime = 1f + random;
				isBouncing = true;
				direction = Vector3.zero;
				
				StartCoroutine(ReturnNormal());
			}
		}
	}
	
	public void Panic () {
		cSpeed *= 3f;
		isPanicking = true;
		panicRoutine = StartCoroutine(Panicking());
	}
	
	public void UnPanic () {
		StopCoroutine(panicRoutine);
		cSpeed = oSpeed;
		direction = Vector3.left;
		self.transform.localScale = Vector3.one;
		isPanicking = false;
	}

	public void SetSpeed (float s) {
		oSpeed = s;
		cSpeed = oSpeed;
	}
	
	public void SetSlow (float sd) {
		slowdown = sd;
	}
	
	IEnumerator Panicking () {
		while (isPanicking) {
			switch (Random.Range(0, 3)) {
				case 0:
					direction = Vector3.right;
					self.transform.localScale = new Vector3(-1f, 1f, 1f);
					break;
				default:
					direction = Vector3.left;
					self.transform.localScale = Vector3.one;
					break;
			}
			yield return new WaitForSeconds(2f);
		}
	}
	
	IEnumerator ReturnNormal () {
		yield return new WaitForSeconds(1.7f);
		direction = Vector3.left;
		canBeKnocked = true;
		startPos = Vector3.zero;
		endPos = Vector3.zero;
	}
	
}

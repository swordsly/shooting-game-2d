﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {

//	public int shotsFired, shotsHit;
	public Text victory;

	// Use this for initialization
	void Start () {
//		shotsFired = GameManager.GetTotalShots();
//		shotsHit = shotsFired - GameManager.GetMissedShots();
		victory.text = GameManager.victory;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			Application.LoadLevel(0);
		}
	}
	
	public void PressMenu () {
		Application.LoadLevel(0);
	}
	
}
